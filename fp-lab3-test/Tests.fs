module Tests

open Xunit
open fp_lab3.Interpolation

type Test() =
    member this.windowData: list<double * double> =
        [ (1., 2.); (3., 4.5); (4., 5.); (5., 5.2) ] |> List.rev

    member this.expectedDataForLinear =
        [| (4.000000, 5.000000)
           (4.200000, 5.040000)
           (4.400000, 5.080000)
           (4.600000, 5.120000)
           (4.800000, 5.160000)
           (5.000000, 5.200000) |]

    member this.expectedDataForLagrange =
        [| (1.000000, 2.000000)
           (1.200000, 2.365200)
           (1.400000, 2.701600)
           (1.600000, 3.010400)
           (1.800000, 3.292800)
           (2.000000, 3.550000)
           (2.200000, 3.783200)
           (2.400000, 3.993600)
           (2.600000, 4.182400)
           (2.800000, 4.350800)
           (3.000000, 4.500000)
           (3.200000, 4.631200)
           (3.400000, 4.745600)
           (3.600000, 4.844400)
           (3.800000, 4.928800)
           (4.000000, 5.000000)
           (4.200000, 5.059200)
           (4.400000, 5.107600)
           (4.600000, 5.146400)
           (4.800000, 5.176800)
           (5.000000, 5.200000) |]

    [<Fact>]
    member this.testLinearInterpolation() =
        this.expectedDataForLinear
        |> Array.iter (fun (x, y) ->
            let actual = linear this.windowData x
            Assert.Equal(y, actual, 0.00001))

    [<Fact>]
    member this.testLagrangeInterpolation() =
        this.expectedDataForLagrange
        |> Array.iter (fun (x, y) ->
            let actual = lagrange this.windowData x
            Assert.Equal(y, actual, 0.00001))
