# fp-lab3

Реализация методов интерполяции: линейного и Лагранжа

## Детали реализации

Линейная интерполяция:
```fsharp
let private linearInterpolation (x1: double, y1: double) (x2: double, y2: double) x =
    if x1 = x2 then
        failwith "x1 and x2 cannot be the same, as this would result in a division by zero."
    else
        y1 + (x - x1) * (y2 - y1) / (x2 - x1)

let linear (input: list<double * double>) (point: double) =
    let x1y1 = input.Head
    let x2y2 = input[1]
    linearInterpolation x1y1 x2y2 point
```

Интерполяция Лагранжа:
```fsharp
let private lagrangeBasis (points: list<double * double>) j x =
    let xj, _ = points[j]

    points
    |> Seq.mapi (fun i (xi, _) -> if i <> j then (x - xi) / (xj - xi) else 1.0)
    |> Seq.fold (*) 1.0

let lagrange (points: list<double * double>) (x: double) =
    points |> Seq.mapi (fun j (_, yi) -> yi * lagrangeBasis points j x) |> Seq.sum
```

Генерация чисел последовательности для функций интерполяции:
```fsharp
let generator (start: double) (step: double) =
    let point (num: int) = start + (double num) * step
    point

let getPointGenerator (points: list<double * double>) step funId =
    let linearSectionStart, _ = points[1]
    let lagrangeSectionStart, _ = points[points.Length - 1]
    let endPoint, _ = points.Head

    match funId with
    | 1 -> (generator linearSectionStart step, int ((endPoint - linearSectionStart) / step))
    | 2 -> (generator lagrangeSectionStart step, int ((endPoint - lagrangeSectionStart) / step))
    | _ -> failwith "Invalid algorithm number"
```

Чтение данных из стандартного ввода:
```fsharp
    let rec inputSequence =
        seq {
            let line = Console.ReadLine()

            if (not (isNull line) && line <> "") then
                let data = line.TrimEnd(';').Split(';')

                if data.Length >= Constants.coordsCount then
                    let x = double data[0]
                    let y = double data[1]
                    yield (x, y)

                yield! inputSequence
            else
                exit 0
        }
```

Окна прочитанных со ввода точек:
```fsharp
let pointsSequence input window =
    let rec getPointsLoop (list: (float * float) list) =
        seq {
            let point = input |> Seq.head

            let newList = updateList list point window

            if newList.Length < Constants.minimalSizeOfWindow then
                yield! getPointsLoop newList
            else
                yield newList
                yield! getPointsLoop newList
        }

    getPointsLoop []
```

Запуск рассчетов и вывод результатов:
```fsharp
let run input (step: double) (window: int) funIds =
    pointsSequence input window
    |> Seq.iter (fun points ->
        let funs =
            Seq.map (fun funId -> (chooseFunc funId points)) funIds
            |> Async.Parallel
            |> Async.RunSynchronously

        funs
        |> Seq.iter (fun (func, funcId) ->
            let pointGenerator, n = getPointGenerator points step funcId

            match funcId, points.Length with
            | 1, _ -> printValues func funcId pointGenerator n
            | 2, l when l = window -> printValues func funcId pointGenerator n
            | _ -> ()))
```

## Запуск

```shell
dotnet run <step> <window> <fun1> [<fun2>]
```

### Интерактивный режим

```shell
$ dotnet run 0.2 4 1 2
1;1
2;3
Linear interpolation result: 
x: 1.200000 y: 1.400000
x: 1.400000 y: 1.800000
x: 1.600000 y: 2.200000
x: 1.800000 y: 2.600000
x: 2.000000 y: 3.000000

4;5
Linear interpolation result: 
x: 2.200000 y: 3.200000
x: 2.400000 y: 3.400000
x: 2.600000 y: 3.600000
x: 2.800000 y: 3.800000
x: 3.000000 y: 4.000000
x: 3.200000 y: 4.200000
x: 3.400000 y: 4.400000
x: 3.600000 y: 4.600000
x: 3.800000 y: 4.800000
x: 4.000000 y: 5.000000

5;6
Linear interpolation result: 
x: 4.200000 y: 5.200000
x: 4.400000 y: 5.400000
x: 4.600000 y: 5.600000
x: 4.800000 y: 5.800000
x: 5.000000 y: 6.000000

Lagrange interpolation result: 
x: 1.200000 y: 1.490667
x: 1.400000 y: 1.932000
x: 1.600000 y: 2.328000
x: 1.800000 y: 2.682667
x: 2.000000 y: 3.000000
x: 2.200000 y: 3.284000
x: 2.400000 y: 3.538667
x: 2.600000 y: 3.768000
x: 2.800000 y: 3.976000
x: 3.000000 y: 4.166667
x: 3.200000 y: 4.344000
x: 3.400000 y: 4.512000
x: 3.600000 y: 4.674667
x: 3.800000 y: 4.836000
x: 4.000000 y: 5.000000
x: 4.200000 y: 5.170667
x: 4.400000 y: 5.352000
x: 4.600000 y: 5.548000
x: 4.800000 y: 5.762667
x: 5.000000 y: 6.000000

7;9
Linear interpolation result: 
x: 5.200000 y: 6.300000
x: 5.400000 y: 6.600000
x: 5.600000 y: 6.900000
x: 5.800000 y: 7.200000
x: 6.000000 y: 7.500000
x: 6.200000 y: 7.800000
x: 6.400000 y: 8.100000
x: 6.600000 y: 8.400000
x: 6.800000 y: 8.700000
x: 7.000000 y: 9.000000

Lagrange interpolation result: 
x: 2.200000 y: 3.233600
x: 2.400000 y: 3.455467
x: 2.600000 y: 3.667200
x: 2.800000 y: 3.870400
x: 3.000000 y: 4.066667
x: 3.200000 y: 4.257600
x: 3.400000 y: 4.444800
x: 3.600000 y: 4.629867
x: 3.800000 y: 4.814400
x: 4.000000 y: 5.000000
x: 4.200000 y: 5.188267
x: 4.400000 y: 5.380800
x: 4.600000 y: 5.579200
x: 4.800000 y: 5.785067
x: 5.000000 y: 6.000000
x: 5.200000 y: 6.225600
x: 5.400000 y: 6.463467
x: 5.600000 y: 6.715200
x: 5.800000 y: 6.982400
x: 6.000000 y: 7.266667
x: 6.200000 y: 7.569600
x: 6.400000 y: 7.892800
x: 6.600000 y: 8.237867
x: 6.800000 y: 8.606400
x: 7.000000 y: 9.000000

```

### Чтение из файла
```shell
$ fp-lab3 % cat linear.txt | dotnet run 0.2 4 1 2
Linear interpolation result: 
x: 1.200000 y: 1.200000
x: 1.400000 y: 1.400000
x: 1.600000 y: 1.600000
x: 1.800000 y: 1.800000
x: 2.000000 y: 2.000000

Linear interpolation result: 
x: 2.200000 y: 2.200000
x: 2.400000 y: 2.400000
x: 2.600000 y: 2.600000
x: 2.800000 y: 2.800000
x: 3.000000 y: 3.000000

Linear interpolation result: 
x: 3.200000 y: 3.200000
x: 3.400000 y: 3.400000
x: 3.600000 y: 3.600000
x: 3.800000 y: 3.800000
x: 4.000000 y: 4.000000

Lagrange interpolation result: 
x: 1.200000 y: 1.200000
x: 1.400000 y: 1.400000
x: 1.600000 y: 1.600000
x: 1.800000 y: 1.800000
x: 2.000000 y: 2.000000
x: 2.200000 y: 2.200000
x: 2.400000 y: 2.400000
x: 2.600000 y: 2.600000
x: 2.800000 y: 2.800000
x: 3.000000 y: 3.000000
x: 3.200000 y: 3.200000
x: 3.400000 y: 3.400000
x: 3.600000 y: 3.600000
x: 3.800000 y: 3.800000
x: 4.000000 y: 4.000000

Linear interpolation result: 
x: 4.200000 y: 4.200000
x: 4.400000 y: 4.400000
x: 4.600000 y: 4.600000
x: 4.800000 y: 4.800000
x: 5.000000 y: 5.000000

Lagrange interpolation result: 
x: 2.200000 y: 2.200000
x: 2.400000 y: 2.400000
x: 2.600000 y: 2.600000
x: 2.800000 y: 2.800000
x: 3.000000 y: 3.000000
x: 3.200000 y: 3.200000
x: 3.400000 y: 3.400000
x: 3.600000 y: 3.600000
x: 3.800000 y: 3.800000
x: 4.000000 y: 4.000000
x: 4.200000 y: 4.200000
x: 4.400000 y: 4.400000
x: 4.600000 y: 4.600000
x: 4.800000 y: 4.800000
x: 5.000000 y: 5.000000

Linear interpolation result: 
x: 5.200000 y: 5.200000
x: 5.400000 y: 5.400000
x: 5.600000 y: 5.600000
x: 5.800000 y: 5.800000
x: 6.000000 y: 6.000000
x: 6.200000 y: 6.200000
x: 6.400000 y: 6.400000
x: 6.600000 y: 6.600000
x: 6.800000 y: 6.800000
x: 7.000000 y: 7.000000

Lagrange interpolation result: 
x: 3.200000 y: 3.200000
x: 3.400000 y: 3.400000
x: 3.600000 y: 3.600000
x: 3.800000 y: 3.800000
x: 4.000000 y: 4.000000
x: 4.200000 y: 4.200000
x: 4.400000 y: 4.400000
x: 4.600000 y: 4.600000
x: 4.800000 y: 4.800000
x: 5.000000 y: 5.000000
x: 5.200000 y: 5.200000
x: 5.400000 y: 5.400000
x: 5.600000 y: 5.600000
x: 5.800000 y: 5.800000
x: 6.000000 y: 6.000000
x: 6.200000 y: 6.200000
x: 6.400000 y: 6.400000
x: 6.600000 y: 6.600000
x: 6.800000 y: 6.800000
x: 7.000000 y: 7.000000

Linear interpolation result: 
x: 7.200000 y: 7.200000
x: 7.400000 y: 7.400000
x: 7.600000 y: 7.600000
x: 7.800000 y: 7.800000
x: 8.000000 y: 8.000000
x: 8.200000 y: 8.200000
x: 8.400000 y: 8.400000
x: 8.600000 y: 8.600000
x: 8.800000 y: 8.800000
x: 9.000000 y: 9.000000
x: 9.200000 y: 9.200000
x: 9.400000 y: 9.400000
x: 9.600000 y: 9.600000
x: 9.800000 y: 9.800000
x: 10.000000 y: 10.000000

Lagrange interpolation result: 
x: 4.200000 y: 4.200000
x: 4.400000 y: 4.400000
x: 4.600000 y: 4.600000
x: 4.800000 y: 4.800000
x: 5.000000 y: 5.000000
x: 5.200000 y: 5.200000
x: 5.400000 y: 5.400000
x: 5.600000 y: 5.600000
x: 5.800000 y: 5.800000
x: 6.000000 y: 6.000000
x: 6.200000 y: 6.200000
x: 6.400000 y: 6.400000
x: 6.600000 y: 6.600000
x: 6.800000 y: 6.800000
x: 7.000000 y: 7.000000
x: 7.200000 y: 7.200000
x: 7.400000 y: 7.400000
x: 7.600000 y: 7.600000
x: 7.800000 y: 7.800000
x: 8.000000 y: 8.000000
x: 8.200000 y: 8.200000
x: 8.400000 y: 8.400000
x: 8.600000 y: 8.600000
x: 8.800000 y: 8.800000
x: 9.000000 y: 9.000000
x: 9.200000 y: 9.200000
x: 9.400000 y: 9.400000
x: 9.600000 y: 9.600000
x: 9.800000 y: 9.800000
x: 10.000000 y: 10.000000

```