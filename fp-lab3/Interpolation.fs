module fp_lab3.Interpolation

let private linearInterpolation (x1: double, y1: double) (x2: double, y2: double) x =
    if x1 = x2 then
        failwith "x1 and x2 cannot be the same, as this would result in a division by zero."
    else
        y1 + (x - x1) * (y2 - y1) / (x2 - x1)

let linear (input: list<double * double>) (point: double) =
    let x1y1 = input.Head
    let x2y2 = input[1]
    linearInterpolation x1y1 x2y2 point


let private lagrangeBasis (points: list<double * double>) j x =
    let xj, _ = points[j]

    points
    |> Seq.mapi (fun i (xi, _) -> if i <> j then (x - xi) / (xj - xi) else 1.0)
    |> Seq.fold (*) 1.0

let lagrange (points: list<double * double>) (x: double) =
    points |> Seq.mapi (fun j (_, yi) -> yi * lagrangeBasis points j x) |> Seq.sum
