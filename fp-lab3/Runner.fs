module fp_lab3.Runner

open Processor


let updateList (list: (float * float) list) (newPoint: float * float) (window: int) =
    match list.Length with
    | 0 -> [ newPoint ]
    | _ -> newPoint :: List.truncate (window - 1) list

let pointsSequence input window =
    let rec getPointsLoop (list: (float * float) list) =
        seq {
            let point = input |> Seq.head

            let newList = updateList list point window

            if newList.Length < Constants.minimalSizeOfWindow then
                yield! getPointsLoop newList
            else
                yield newList
                yield! getPointsLoop newList
        }

    getPointsLoop []

let run input (step: double) (window: int) funIds =
    pointsSequence input window
    |> Seq.iter (fun points ->
        let funs =
            Seq.map (fun funId -> (chooseFunc funId points)) funIds
            |> Async.Parallel
            |> Async.RunSynchronously

        funs
        |> Seq.iter (fun (func, funcId) ->
            let pointGenerator, n = getPointGenerator points step funcId

            match funcId, points.Length with
            | 1, _ -> printValues func funcId pointGenerator n
            | 2, l when l = window -> printValues func funcId pointGenerator n
            | _ -> ()))
