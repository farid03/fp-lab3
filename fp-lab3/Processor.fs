module fp_lab3.Processor

open Interpolation

let getFuncName (funcId: int) : string =
    match funcId with
    | 1 -> "Linear interpolation"
    | 2 -> "Lagrange interpolation"
    | _ -> failwith "Invalid algorithm number"

let generator (start: double) (step: double) =
    let point (num: int) = start + (double num) * step
    point

let getPointGenerator (points: list<double * double>) step funId =
    let linearSectionStart, _ = points[1]
    let lagrangeSectionStart, _ = points[points.Length - 1]
    let endPoint, _ = points.Head

    match funId with
    | 1 -> (generator linearSectionStart step, int ((endPoint - linearSectionStart) / step))
    | 2 -> (generator lagrangeSectionStart step, int ((endPoint - lagrangeSectionStart) / step))
    | _ -> failwith "Invalid algorithm number"

let chooseFunc (funId: int) (input: list<double * double>) =
    async {
        let func =
            match funId with
            | 1 -> linear input
            | 2 -> lagrange input
            | _ -> failwith "Invalid algorithm number"

        return (func, funId)
    }

let printValues (func: double -> double) funcId (pointGenerator: int -> double) n =
    { 0..n }
    |> Seq.map (fun i -> $"x: %f{pointGenerator i} y: %f{func (pointGenerator i)}\n")
    |> Seq.fold (+) ""
    |> fun s -> printfn $"{getFuncName funcId} result: \n{s}"
