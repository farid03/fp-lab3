module fp_lab3.Program

open System
open Runner

module Program =

    let parseInput (argv: string array) =
        let funIds =
            if argv.Length = 4 then
                [ int argv[2]; int argv[3] ]
            else
                [ int argv[2] ]

        let step, window = double argv[0], int argv[1]

        assert (window >= Constants.minimalSizeOfWindow)

        step, window, funIds


    let rec inputSequence =
        seq {
            let line = Console.ReadLine()

            if (not (isNull line) && line <> "") then
                let data = line.TrimEnd(';').Split(';')

                if data.Length >= Constants.coordsCount then
                    let x = double data[0]
                    let y = double data[1]
                    yield (x, y)

                yield! inputSequence
            else
                exit 0
        }

    [<EntryPoint>]
    let main argv =
        match argv.Length with
        | 3
        | 4 ->
            let step, window, funIds = parseInput argv
            run inputSequence step window funIds
        | _ ->
            printfn "Invalid number of arguments. Please use the following format:"
            printf "\tdotnet run <step> <window> <fun1>\n\tor\n\tdotnet run <step> <window> <fun1> <fun2>"

        0
